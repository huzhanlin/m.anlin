module.exports = {
  productionSourceMap: false,
  css: {
    loaderOptions: {
      scss: {
        prependData: `@import "@/assets/css/global.scss";`
      }
    }
  }
}
