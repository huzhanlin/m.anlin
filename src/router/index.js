import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'index',
    redirect: '/blogs'
  },
  {
    path: '/blogs',
    name: 'Blogs',
    component: () => import('@/views/blogs'),
    meta: { title: '博客' }
  },
  {
    path: '/music',
    name: 'Music',
    component: () => import('@/views/music'),
    meta: { title: '音乐' }
  },
  {
    path: '/mine',
    name: 'Mine',
    component: () => import('@/views/mine'),
    meta: { title: '我的' }
  },
  {
    path: '/noteList/:id',
    name: 'NoteListId',
    component: () => import('@/views/noteList'),
    meta: { title: '个人笔记' }
  },
  {
    path: '/blogdetail/:id',
    name: 'BlogdetailId',
    component: () => import('@/views/blogdetail'),
    meta: { title: '博客详情' }
  },
  {
    path: '/bloglist/:id',
    name: 'Bloglist',
    component: () => import('@/views/bloglist'),
    meta: { title: '博客列表' }
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
