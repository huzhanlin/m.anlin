import router from '@/router'
import store from '@/store'
router.beforeEach((to, from, next) => {
  next()
})

router.afterEach((to, from) => {
  store._mutations.setCurrentPage[0](to)
  if (to.meta) document.title = to.meta.title
})
