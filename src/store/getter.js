export default {
  unread(state) {
    const message = state.message
    let number = 0
    for (const i of message) {
      if (!i.status) {
        number += 1
      }
    }
    return number
  }
}
