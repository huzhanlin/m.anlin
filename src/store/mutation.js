import db from '@/utils/db.js'

export default {
  setCurrentPage(state, data) {
    state.currentPage = data
  },

  setUserinfo(state, data) {
    db.userLoc.set('userinfo', data)
    state.userinfo = data
  },

  setMessage(state, data) {
    state.message = data
  },

  clearUserinfo(state, data) {
    db.userLoc.clear('userinfo')
    state.userinfo = ''
  }
}
