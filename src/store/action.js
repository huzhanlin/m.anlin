import RestApi from '@/api'
import db from '@/utils/db.js'

class common extends RestApi {
  static get baseUrl() {
    return {
      banner: `${this.reqBaseUrl}/user/alldata/getbannerData`,
      hotBlogs: `${this.reqBaseUrl}/user/alldata/getAllRzdata?limit=8&skip=0`,
      blogDetail: `${this.reqBaseUrl}/user/alldata/getskillinid`,
      rzDetail: `${this.reqBaseUrl}/user/alldata/getidrzinuser`,
      blogsDataType: `${this.reqBaseUrl}/user/alldata/getallskilldata`,
      login: `${this.reqBaseUrl}/user/log_operation/login`,
      userinfo: `${this.reqBaseUrl}/user/userdata/getuserdata`,
      yzm: `${this.reqBaseUrl}/user/log_operation/getemailyzm`,
      update: `${this.reqBaseUrl}/user/log_operation/update_pw`,
      find: `${this.reqBaseUrl}/user/log_operation/find_pw`,
      declaration: `${this.reqBaseUrl}/user/userdata/getdeclarationData`,
      message: `${this.reqBaseUrl}/user/alldata/getMessage`,
      noteBook: `${this.reqBaseUrl}/user/alldata/getnotebook`,
      setNoteBook: `${this.reqBaseUrl}/user/alldata/setnotebook`,
      hot: `${this.musicUrl}/search/hot`,
      musicBanner: `${this.musicUrl}/banner`,
      list: `${this.musicUrl}/artist/list`
    }
  }
}

export default {
  banner(ctx) {
    return common.get('banner')
  },
  blog(ctx) {
    return common.get('hotBlogs')
  },
  detail(ctx, data) {
    return common.get('blogDetail', data)
  },
  rzDetail(ctx, data) {
    return common.get('rzDetail', data)
  },
  blogsDataType(ctx, data) {
    return common.get('blogsDataType', data)
  },
  login(ctx, data) {
    return common.post('login', data)
  },
  userinfo(ctx, data) {
    return common.get('userinfo', data)
  },
  yzm(ctx, data) {
    return common.get('yzm', data)
  },
  update(ctx, data) {
    return common.post('update', data)
  },
  find(ctx, data) {
    return common.post('find', data)
  },
  declaration(ctx, data = {}) {
    const userName = db.userLoc.get('userinfo').userName
    data.userName = userName
    return common.get('declaration', data)
  },

  message(ctx, data = {}) {
    const userName = db.userLoc.get('userinfo').userName
    data.userName = userName
    return common.get('message', data).then(res => {
      if (res.data.ok === 1) {
        ctx.commit('setMessage', res.data.data)
      }
    })
  },

  noteBook(ctx, data) {
    return common.get('noteBook', data)
  },

  setNoteBook(ctx, data) {
    return common.get('setNoteBook', data)
  },

  getHot(ctx, data) {
    return common.get('hot', data)
  },

  getBanner(ctx, data) {
    return common.get('musicBanner', data)
  },

  getList(ctx, data) {
    data.limit = 6
    data.offset = 0
    return common.get('list', data)
  }

}
