import axiosApi from 'axios'

export default class RestApi {
  static get reqBaseUrl() {
    return 'http://106.12.122.161:3001'
  }
  static get musicUrl() {
    return 'http://106.12.122.161:5000'
  }

  static get(url, data) {
    url = this.baseUrl[url]
    if (data) {
      url += '?'
      Object.entries(data).forEach((item, index) => {
        if (index !== 0) {
          url += '&' + item[0] + '=' + item[1]
        } else {
          url += item[0] + '=' + item[1]
        }
      })
    }
    return axiosApi({
      method: 'get',
      url
    })
  }

  static post(url, data) {
    return axiosApi({
      method: 'post',
      url: this.baseUrl[url],
      data
    })
  }

  static put(url, data) {
    return axiosApi({
      method: 'put',
      url: this.baseUrl[url],
      data
    })
  }

  static delete(url, data) {
    return axiosApi({
      method: 'delete',
      url: this.baseUrl[url],
      data
    })
  }
}
