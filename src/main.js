import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// 引入vant
import { Tabs, Tab, Picker, Popup, Search, Notify, Overlay, Tabbar, TabbarItem, NavBar, Icon, Swipe, SwipeItem, Grid, GridItem, Image as VanImage, Cell, CellGroup, Badge, Sticky, List, Tag, Form, Field, Button, RadioGroup, Radio, Toast, NoticeBar, SwipeCell } from 'vant'
import 'vant/lib/index.css'
Vue.use(Tab)
Vue.use(Tabs)
Vue.use(Picker)
Vue.use(Popup)
Vue.use(Search)
Vue.use(Notify)
Vue.use(Overlay)
Vue.use(Tabbar)
Vue.use(TabbarItem)
Vue.use(NavBar)
Vue.use(Icon)
Vue.use(Swipe)
Vue.use(SwipeItem)
Vue.use(Grid)
Vue.use(GridItem)
Vue.use(VanImage)
Vue.use(Cell)
Vue.use(CellGroup)
Vue.use(Badge)
Vue.use(Sticky)
Vue.use(List)
Vue.use(Tag)
Vue.use(Form)
Vue.use(Field)
Vue.use(Button)
Vue.use(RadioGroup)
Vue.use(Radio)
Vue.use(Toast)
Vue.use(NoticeBar)
Vue.use(SwipeCell)
// 引入路由导航
import '@/router/permission.js'

// 引入axios
import $api from '@/api'
Vue.prototype.$axios = { $api }

// 引入css文件
import '@/assets/css/index.css'
import gb from '@/assets/css/global.scss'
Vue.prototype.$gb = gb

// 引入工具函数
import utils from '@/utils'
Vue.prototype.$t = utils

import '@/assets/css/marked.sass'
import marked from 'marked'
Vue.prototype.$md = marked
// 引入ckeditor富文本编辑器
// import CKEditor from '@ckeditor/ckeditor5-vue2'
// Vue.use(CKEditor)
// import ClassicEditor from '@ckeditor/ckeditor5-build-decoupled-document'
// Vue.prototype.$ed5 = ClassicEditor

// 添加本地缓存的事件
import db from '@/utils/db.js'
Vue.prototype.$db = db

Vue.filter('uri', (data) => {
  const imgUri = gb.imgUri.replace(/"/g, '')
  return imgUri + data
})

Vue.filter('imageUri', (data) => {
  const imgUri = gb.imageUri.replace(/"/g, '')
  return imgUri + data
})

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
